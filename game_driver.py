#!/usr/bin/env python
# Ancestral Pizza - A game of evolution and hunger
# Sprites by Marcy Galloway
# Code by Jeff Ladish and Marcy Galloway
#
# Description: The game is designed to be a 2D action RPG, where a player controls a human and has to survive in a harsh environment. The player spends "Food currency" as the game progresses in order to unlock more weapons and technology.


##TODO ##
##SGE has a way to do view in their View.py class
##I would prefer to not have to use SGE, so another
##way is preferable.

import random, os.path

#import basic pygame modules
import pygame
from pygame.locals import *


from map_gen import MapGen
from rabbit import Rabbit
from wolf import Wolf

#see if we can load more than standard BMP
if not pygame.image.get_extended():
    raise SystemExit("Sorry, extended image module required")

main_dir = os.path.split(os.path.abspath(__file__))[0]

# STATIC VARIABLES -- Many for future implementation

MAP_WIDTH = 1000
MAP_HEIGHT = 700
SCREENRECT = Rect(0, 0, MAP_WIDTH, MAP_HEIGHT)

#Speed
HUMAN_SPEED = -4
HUMAN_SPRINT_SPEED = -10
WOLF_SPEED = -10
RABBIT_SPEED = -2
SPEAR_SPEED = -5

# Health and damage will be the same scale. If a punch does 1 damage, and an animal has 5 health, it will take 5 punches to kill the animal.

# Health
RABBIT_HEALTH = 6
DEER_HEALTH = 10
WOLF_HEALTH = 15


# Range - this might be done by hit boxes, so may become obselete
PUNCH_RANGE = 1
CLUB_RANGE = 2
AXE_RANGE = 2

# Food cost of 

# These numbers are made up. Once the code is determined, they will had to be tested and ade sensible.
THROW_RANGE = 7
SPEAR_RANGE = 15
BOW_RANGE = 30

PUNCH_DAMAGE = 2
CLUB_DAMAGE = 2
AXE_DAMAGE = 3
ROCK_DAMAGE = 1
SPEAR_DAMAGE = 5

# Recharges in seconds

SPRINT_DURATION = 10
SPRINT_RECHARGE = 15

ROCK_THROW_RECHARGE = 2
SPEAR_RECHARGE = 4
ARROW_RECHARGE = 5


#Max inventory
MAX_ROCKS = 20
MAX_SPEARS = 10
MAX_ARROWS = 20

START_FOOD = 100
START_HEALTH = 20
START_ROCKS = 5

MAX_SHOTS = 2

# NON STATIC VARIABLES - STARTING VALUES

FOOD = 100



# Functions


def load_image(file):
    "loads an image, prepares it for play"
    file = os.path.join(main_dir, 'data', file)
    try:
        surface = pygame.image.load(file)
    except pygame.error:
        raise SystemExit('Could not load image "%s" %s'%(file, pygame.get_error()))
    return surface.convert()

def load_images(*files):
    imgs = []
    for file in files:
        imgs.append(load_image(file))
    return imgs

# Load Sprites

class Player(pygame.sprite.Sprite):
    bounce = 24
    speed = HUMAN_SPEED
    gun_offset = -11
    win_var = 0
    images = []
    #used to calculate spear fire rate
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.reloading = 0
        self.origtop = self.rect.top
        self.facing = "up"
        self.spear_time = 0
        self.food_value = 10

        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        # Set speed vector
        self.change_x = 0
        self.change_y = 0


    def changespeed(self, x, y):
        # Change the speed of the player
        self.change_x += x
        self.change_y += y

    def update(self):
        # Find a new position for the player
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        self.spear_time += 1

    def gunpos(self):
        pos = self.gun_offset + self.rect.centerx
        return pos, self.rect.top

class Tree(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0

class Tuber(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0

class StickPile(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0


class Stick(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0

class RabbitMeat(pygame.sprite.Sprite):
    def __init__(self,actor):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect(center=actor.rect.center)
        self.frame = 0

class Pizza(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0

class WinScreen(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0

class Spear(pygame.sprite.Sprite):
    speed = SPEAR_SPEED
    images = []
    initiate_val = 1
    def __init__(self, pos):
        self.move_dir = Player.facing
        pygame.sprite.Sprite.__init__(self, self.containers)
        if Player.facing == "right":
            self.image = self.images[3]
        elif Player.facing == "up":
            self.image = self.images[0]
        elif Player.facing == "down":
            self.image = self.images[1]
        elif Player.facing == "left":
            self.image = self.images[2]

        self.rect = self.image.get_rect(midbottom=pos)

    def update(self):
        if self.initiate_val:
            if Player.facing == "right":
                self.mov_dir = "right"
                self.rect.move_ip(-self.speed,0)
                self.initiate_val = 0
            elif Player.facing == "down":
                self.move_dir = "down"
                self.rect.move_ip(0,-self.speed)
                self.initiate_val = 0
            elif Player.facing == "left":
                self.move_dir = "left"
                self.rect.move_ip(self.speed,0)
                self.initiate_val = 0
            elif Player.facing == "up":
                self.mov_dir = "up"
                self.rect.move_ip(0,self.speed)
                self.initiate_val = 0
        else:
            if self.move_dir == "right":
                self.rect.move_ip(-self.speed,0)
            elif self.move_dir == "up":
                self.rect.move_ip(0,self.speed)
            elif self.move_dir == "down":
                self.rect.move_ip(0,-self.speed)
            elif self.move_dir == "left":
                self.rect.move_ip(self.speed,0)                    
        if self.rect.x <= 0:
            self.kill()
        if self.rect.y <= 0:
            self.kill()
        if self.rect.x >= 1000:
            self.kill()
        if self.rect.y >= 700:
            self.kill()

class Score():
    def __init__(self):
        self.food_value = 10
        self.rabbit_num = MapGen.RABBITNUM
        self.tuber_num = MapGen.TUBORNUM

def main(winstyle = 0):
    pygame.init()
    if pygame.mixer and not pygame.mixer.get_init():
        print ('Warning, no sound')
        pygame.mixer = None

    # Set the display mode
    winstyle = 0  # FULLSCREEN
    bestdepth = pygame.display.mode_ok(SCREENRECT.size, winstyle, 32)
    screen = pygame.display.set_mode(SCREENRECT.size, winstyle, bestdepth)

    #Load images, assign to sprite classes

    #Player images
    img = load_image('player1.PNG')
    img = pygame.transform.scale(img,(52,28))
    Player.images = [img, pygame.transform.flip(img, 0, 1),pygame.transform.rotate(img, 90), pygame.transform.rotate(img, 270)]

    #Wolf images
    img = load_image('wolf1.PNG')
    img = pygame.transform.scale(img,(33,93))
    Wolf.images = [img, pygame.transform.flip(img, 0, 1), pygame.transform.rotate(img, 90), pygame.transform.rotate(img, 270),pygame.transform.rotate(img, 45),pygame.transform.rotate(img, 135),pygame.transform.rotate(img, 225),pygame.transform.rotate(img, 315)]

    #Tree images
    img = load_image('tree.PNG')
    Tree.images = [pygame.transform.scale(img,(95,87))]
    img = load_image('tuber.PNG')
    img = pygame.transform.scale(img,(20,19))

    #Tuber images
    Tuber.images = [img]

    #Rabbit images
    img = load_image('rabbit1.PNG')
    img = pygame.transform.scale(img,(12,22))
    Rabbit.images = [img, pygame.transform.flip(img, 0, 1), pygame.transform.rotate(img, 90), pygame.transform.rotate(img, 270),pygame.transform.rotate(img, 45),pygame.transform.rotate(img, 135),pygame.transform.rotate(img, 225),pygame.transform.rotate(img, 315)]

    #Spear images
    img = load_image('spear.PNG')
    img = pygame.transform.scale(img,(11,59))
    Spear.images = [img, pygame.transform.flip(img, 0, 1), pygame.transform.rotate(img, 90), pygame.transform.rotate(img, 270)]

    #Rabbit meat images
    img = load_image('rabbitmeats.PNG')
    img = pygame.transform.scale(img,(20,19))
    RabbitMeat.images = [img]

    #Pizza images
    img = load_image('pizza.gif')
    img = pygame.transform.scale(img,(100,100))
    Pizza.images = [img]

    #Background images
    img = load_image('winscreen.gif')
    img = pygame.transform.scale(img,(700,700))
    WinScreen.images = [img]

    #decorate the game window
 
    black = (0,0,0)
    myfont = pygame.font.SysFont("Comic Sans MS", 30)
    label = myfont.render("Collect all the tubors for a special surprise!", 1, black)
    # put the label object on the screen at point x=100, y=100

    #create the background, tile the bgd image
    bgdtile = load_image('background.png')
    bgdtile = pygame.transform.scale(bgdtile,(1000,1000))
    background = pygame.Surface(SCREENRECT.size)
    for x in range(0, SCREENRECT.width, bgdtile.get_width()):
        background.blit(bgdtile, (x, 0))
    screen.blit(background, (0,0))
    screen.blit(label, (10, 675))
    pygame.display.flip()

    # Initialize Game Groups
    trees = pygame.sprite.Group()
    tubers = pygame.sprite.Group()
    rabbits = pygame.sprite.Group()
    wolves = pygame.sprite.Group()
    sticks = pygame.sprite.Group()
    stickPiles = pygame.sprite.Group()
    spears = pygame.sprite.Group()
    rabbitMeats = pygame.sprite.Group()
    pizzas = pygame.sprite.Group()
    winscreen = pygame.sprite.Group()
    all = pygame.sprite.RenderUpdates()

    #assign default groups to each sprite class
    Player.containers = all
    Tree.containers = trees, all
    Tuber.containers = tubers, all
    Rabbit.containers = rabbits, all
    Wolf.containers = wolves, all
    Stick.containers = sticks, all
    StickPile.containers = stickPiles, all
    Spear.containers = spears, all
    RabbitMeat.containers = rabbitMeats, all
    Pizza.containers = pizzas, all
    WinScreen.containers = winscreen, all


    #Create Some Starting Values
    player_food = START_FOOD
    inv_arrows = 0
    inv_spears = 0
    clock = pygame.time.Clock()
    score = Score()
    Player.facing = "up"
  


    #generate the lists containing the random starting values
    map_gen_instance = MapGen()
    final_placement_list = map_gen_instance.map_gen()
    player = Player(500,500)

    final_tree_list = final_placement_list[0]
    final_tubor_list = final_placement_list[1]
    final_stickpile_list = final_placement_list[2]
    final_rabbit_list = final_placement_list[3]
    final_wolf_list = final_placement_list[4]

    #place the objects in the room
    for cords in final_tree_list:
        Tree(cords[0],cords[1])
    for cords in final_tubor_list:
        Tuber(cords[0],cords[1])
    for cords in final_rabbit_list:
        Rabbit(cords[0],cords[1])
    for cords in final_wolf_list:
        Wolf(cords[0],cords[1])

    
    

    while player.alive():
        #get input
        for event in pygame.event.get():
            if event.type == QUIT or \
                (event.type == KEYDOWN and event.key == K_ESCAPE):
                    return
            # Set the speed based on the key pressed
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    player.image = player.images[2]
                    player.changespeed(-2, 0)
                    Player.facing = "left"
                elif event.key == pygame.K_RIGHT:
                    player.image = player.images[3]
                    player.changespeed(2, 0)
                    Player.facing = "right"
                elif event.key == pygame.K_UP:
                    player.image = player.images[0]
                    player.changespeed(0, -2)
                    Player.facing = "up"
                elif event.key == pygame.K_DOWN:
                    player.image = player.images[1]
                    player.changespeed(0, 2)
                    Player.facing = "down"
                elif event.key == pygame.K_SPACE:
                    if len(spears) <= MAX_SHOTS and player.spear_time > 20 and score.tuber_num < 1: 
                        Spear(player.gunpos())
                        player.spear_time = 0

            # Reset speed when key goes up
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    player.changespeed(2, 0)
                elif event.key == pygame.K_RIGHT:
                    player.changespeed(-2, 0)
                elif event.key == pygame.K_UP:
                    player.changespeed(0, 2)
                elif event.key == pygame.K_DOWN:
                    player.changespeed(0, -2)

        # This actually moves the player block based on the current speed
        player.update()

        # clear/erase the last drawn sprites
        all.clear(screen, background)

        #update all the sprites
        all.update()

        # Detect collisions
        for tuber in pygame.sprite.spritecollide(player, tubers, 1):
            score.tuber_num = score.tuber_num - 1
            if score.tuber_num < 1:
                label = myfont.render("Spears unlocked: press space", 1, black)
                screen.blit(label, (10, 675))

        for wolf in pygame.sprite.spritecollide(player, wolves, 1):
            player.kill()

        for pizza in pygame.sprite.spritecollide(player, pizzas, 1):
            label = myfont.render("You've won the game!", 1, black)
            screen.blit(label, (10, 675))
            WinScreen(1,1)
            Player.win_var = 1

        for rabbitMeat in pygame.sprite.spritecollide(player, rabbitMeats, 1):
            score.rabbit_num = score.rabbit_num - 1
            if score.rabbit_num < 1:
                final_pizza = Pizza(500,350)
                label = myfont.render("Ancestral pizza unlocked: eat it", 1, black)
                screen.blit(label, (10, 675))
        
        for spear in pygame.sprite.groupcollide(spears,wolves, 1,1).keys():
            Wolf.feels = "angry"
            Wolf(950,100)
            Wolf(950,200)
            Wolf(950,300)
            Wolf(950,400)
            Wolf(950,500)
            Wolf(950,600)
            Wolf(50,100)
            Wolf(50,200)
            Wolf(50,300)
            Wolf(50,400)
            Wolf(50,500)
            Wolf(50,600)

        for rabbit in pygame.sprite.groupcollide(rabbits,spears,1,1).keys():
            rabbit_num = score.rabbit_num - 1
            RabbitMeat(rabbit)

        #draw the scene
        dirty = all.draw(screen)
        pygame.display.update(dirty)        
 
        #cap the framerate
        clock.tick(40)

if __name__ == '__main__': main()

