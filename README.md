# Ancestral Pizza

#### Game description ####
The game is designed to be a 2D action RPG, where a player controls a human and has to survive in a harsh environment. 
Currently, the player's objective is to collect all the carrots to unlock spears, spear all the rabbits without hitting the wolves, and then eat the final  pizza. In the future, the game will include the following features:

The player will spend "Food currency" as the game progresses in order to unlock more weapons and technology.

Players will begin with the ability to pick up items: rocks on the ground, tubors in the ground, sticks from bushes, and meat from (hunted) rabbits. They will have a ranged and melee attack. The melee will begin as a punch and the ranged will begin as rocks that can be thrown. A stacked inventory in the style of minecraft
would be great, but not necessary.

The first weapons that will be unlocked are a club and a bow. A club will cost 500 food and require 10 sticks, and a bow will cost 1,000 food and require 15 sticks (maybe eventually it will require string, but that is outside the scope right now).

Technology will be bought around the fire.


# Todo

#### SPRITES ####
- [x] Human
- [x] Human w/ spear
- [ ] Human w/ sling
- [x] Wolf1
- [x] Rabbit1
- [ ] Human with Spear
- [x] Tuber
- [x] Rabbit Meat
- [x] Tree
- [ ] Bush
- [ ] Cave
- [ ] Fire
- [ ] Rock
- [ ] Food meter
- [ ] Health meter

#### CODING ####
- [x] Set up Main
- [x] Initiate Sprites
- [x] Set up room and load backgrounds
- [x] Make object classes
- [x] Set up random spawn of objects
- [x] Set up controls
- [x] Make collisions
- [ ] Make food meter
- [x] Code AI movements
- [ ] Code fire and cave interactions


####Github help####
if you have git on your linux or mac computer you can control git via the terminal.

 * git clone https://github.com/UserJeff/AncestralPizza
makes a copy of the repository on your computer. Make sure you do this in a folder you don't mind having things dumped in.

 * git pull 
checks the website for new changes and downloads them to the repository on your computer.

 * git commit -am 'YourMessageHere'
Saves a commit which can be pushed latter. You should put a message explining what changed in the quotes.

 * git push
Pushes the commit you just made to the repository on github.

 * git status
shows you the current status of the repository, including which files are or are not being tracked.

 * git add FileNamehere
adds files to the repository so they can be committed and pushed.
