#!/usr/bin/env python
# MapGen class controls where objects will spawn and in what density

import random

class MapGen():

        #To change game mode, comment out current mode and uncomment desired mode

        #MODE = "easy"
        MODE = "medium"
        #MODE = "hard"
        #MODE = "very hard"

        MAP_WIDTH = 950
        MAP_HEIGHT = 650
        TREENUM = 10        
        STICKPILENUM = 5
        
        
        if MODE == "easy":
            TUBORNUM = 5
            WOLFNUM = 1
            RABBITNUM = 3
        elif MODE == "medium":
            TUBORNUM = 10
            WOLFNUM = 2
            RABBITNUM = 8
        elif MODE == "hard":
            TUBORNUM = 20
            WOLFNUM = 3
            RABBITNUM = 10
        elif MODE == "very hard":
            TUBORNUM = 20
            WOLFNUM = 4
            RABBITNUM = 13

        PLACEMENT_BOX = 100
        TREE_PLACEMENT_BOX = 200

        tree_list = [(200,200)]
        tubor_list = []
        stick_pile_list = []
        rabbit_list = []
        wolf_list = []
        sum_list = [(500,500)]

        def __init__(self):
            some_variable = "some_value"

        def map_gen(self):
            #generate trees
            for i in range(MapGen.TREENUM):
                treecords = self.gen_new_cords()
                MapGen.tree_list.append(treecords)
            #generate tubors
            for i in range(MapGen.TUBORNUM):
                tuborcords = self.gen_new_cords()
                MapGen.tubor_list.append(tuborcords)
                MapGen.sum_list.append(tuborcords)
            #generate stick piles
            for i in range(MapGen.STICKPILENUM):
                stickpilecords = self.gen_new_cords()
                MapGen.stick_pile_list.append(stickpilecords)
                MapGen.sum_list.append(stickpilecords)
            #generate rabbits
            for i in range(MapGen.RABBITNUM):
                rabbitcords = self.gen_new_cords()
                MapGen.rabbit_list.append(rabbitcords)
                MapGen.sum_list.append(rabbitcords)
            #generate wolves
            for i in range(MapGen.WOLFNUM):
                wolfcords = self.gen_new_cords()
                MapGen.wolf_list.append(wolfcords)
                MapGen.sum_list.append(wolfcords)

            #creates list of coordinates for each type of object that spawns when the map is created
            game_gen_list = [MapGen.tree_list,MapGen.tubor_list,MapGen.stick_pile_list,MapGen.rabbit_list,MapGen.wolf_list]
            return game_gen_list
        def gen_new_cords(self):
            x = 100
            y = 100
            while not self.cords_occupied(x,y):
                x = int(random.random() * MapGen.MAP_WIDTH)
                y = int(random.random() * MapGen.MAP_HEIGHT)
            return (x,y)

        def cords_occupied(self,x,y):
            #Check if placement conflicts with trees
            #Needs refinement because trees still overlap
            treex = x - MapGen.TREE_PLACEMENT_BOX / 2
            treey = y - MapGen.TREE_PLACEMENT_BOX / 2
            for a in range(MapGen.TREE_PLACEMENT_BOX):
                for b in range(MapGen.TREE_PLACEMENT_BOX):
                    treex = treex + 1
                    treey = treey + 1
                    for cords in MapGen.tree_list:
                        if (treex,treey) == cords:
                            return False

            #Check if placement conflicts with other things
            x = x - MapGen.PLACEMENT_BOX / 2
            y = y - MapGen.PLACEMENT_BOX / 2
            for a in range(MapGen.PLACEMENT_BOX):
                for b in range(MapGen.PLACEMENT_BOX):
                    x = x + 1
                    y = y + 1
                    for cords in MapGen.sum_list:
                        if (x,y) == cords:
                            return False
            return True




            
    


