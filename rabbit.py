#!/usr/bin/python

import random
import pygame

RABBIT_SPEED = -2

MAP_WIDTH = 1000
MAP_HEIGHT = 700

class Rabbit(pygame.sprite.Sprite):
    speed = RABBIT_SPEED
    move_steps = 30
    directions = ["right","down","left","up","right_down","right_up","left_down","left_up","rest","rest","rest","rest","rest","rest","rest"]
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = self.images[0]
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.frame = 0
        self.mov_count = 0
        self.mov_dir = "right"

    def update(self):
        if self.mov_count > Rabbit.move_steps:
            self.mov_count = 0
            #If outside the screen to the left or up
            if self.rect.x < 50:
                if self.rect.y < 50:
                    self.mov_dir = "right_down"
                else: self.mov_dir = "right"
            elif self.rect.y < 50:
                self.mov_dir = "down"

            #If outside the screen to the right or down
            elif self.rect.x > MAP_WIDTH - 50:
                if self.rect.y > MAP_HEIGHT - 50:
                    self.mov_dir = "left_up"
                else: self.mov_dir = "left"
            elif self.rect.y > MAP_HEIGHT - 50:
                self.mov_dir = "up"      

            #If not outside the screen
            else: self.mov_dir = Rabbit.directions[int(random.random() * 14)]

        #If the mov_count is not up to mov_steps, increase the count
        else:
            self.mov_count = self.mov_count + 1
        
        if self.mov_dir == "right":
            self.rect.move_ip(-self.speed,0)
            self.image = self.images[3]
        if self.mov_dir == "down":
            self.rect.move_ip(0,-self.speed)
            self.image = self.images[1]
        if self.mov_dir == "left":
            self.rect.move_ip(self.speed,0)
            self.image = self.images[2]
        if self.mov_dir == "up":
            self.rect.move_ip(0,self.speed)
            self.image = self.images[0]
        if self.mov_dir == "right_up":
            self.rect.move_ip(-self.speed/2,self.speed/2)
            self.image = self.images[7]
        if self.mov_dir == "right_down":
            self.rect.move_ip(-self.speed/2,-self.speed/2)
            self.image = self.images[6]
        if self.mov_dir == "left_up":
            self.rect.move_ip(self.speed/2,self.speed/2)
            self.image = self.images[4]
        if self.mov_dir == "left_down":
            self.rect.move_ip(self.speed/2,-self.speed/2)
            self.image = self.images[5]
        if self.mov_dir == "rest":
            self.rect.move_ip(0,0)